﻿(function () {
    'use strict';

    angular
        .module("appMeli")
        .controller("appController",
                    ["$scope",
                    "userService", 
                    function ($scope, userService) {
                       
                        $scope.login = function (isValid) {

                                if (isValid) {
                                    var usuario = {
                                        username: $scope.username,
                                        password: $scope.password,
                                    };
                                    if (userService.login(usuario)) {
                                        $scope.state = "autorizado";
                                    }
                                    else {
                                        $scope.loginError = "Error! Credenciales incorrectas. Por favor reintente."
                                    }
                                }
                                else {
                                    $scope.loginError = "Error! Por favor complete los campos requeridos";
                                }


                            };
                        }
                    ]);


    //function appController($scope) {
    //    $scope.state = 'noAutorizado';

    //    $scope.login = function () {
    //        var username = $scope.username;
    //        var password = $scope.password;

    //        if (username == "admin" && password == "admin") {
    //            $scope.state = 'autorizado';
    //            $scope.isLoggedIn = true;
    //        }
    //        else {
    //            $scope.isLoggedIn = false;
    //            $scope.msjLoggedIn = "Credenciales incorrectas";
    //        }
    //    }
    //}

}());