﻿(function () {
    'use strict';

    angular
        .module("appMeli")
        .controller("meliFrameworkController",
                    ['$scope', '$window', '$timeout', '$rootScope', '$location',
                        meliFrameworkController]
        );

    function meliFrameworkController($scope, $window, $timeout, $rootScope, $location) {

        $scope.isMenuVisible = true;
        $scope.isMenuButtonVisible = true;
        $scope.isMenuVertical = true;

        $scope.$on('meli-menu-item-selected-event', function (evt, data) {
            checkWidth();
            broadcastMenuState();
            $($window).trigger('resize');
        });

        $scope.$on('meli-menu-orientation-changed-event', function (evt, data) {
            $scope.isMenuVertical = data.isMenuVertical;
            $timeout(function () {
                $($window).trigger('resize');
            }, 0);
        });

        $($window).on('resize.meliFramework', function () {
            $scope.$apply(function () {
                checkWidth();
                broadcastMenuState();
            });
        });
        $scope.$on("$destroy", function () {
            $($window).off("resize.meliFramework"); // remove the handler added earlier
        });

        var checkWidth = function () {
            var width = Math.max($($window).width(), $window.innerWidth);
            $scope.isMenuVisible = (width >= 768);
            $scope.isMenuButtonVisible = !$scope.isMenuVisible;
        };

        $scope.menuButtonClicked = function () {
            $scope.isMenuVisible = !$scope.isMenuVisible;
            broadcastMenuState();
            //$scope.$apply();
        };

        var broadcastMenuState = function () {
            $rootScope.$broadcast('meli-menu-show',
                {
                    show: $scope.isMenuVisible,
                    isVertical: $scope.isMenuVertical,
                    allowHorizontalToggle: !$scope.isMenuButtonVisible
                });
        };

        $timeout(function () {
            checkWidth();
        }, 0);

        }

}());