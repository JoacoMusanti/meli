﻿(function () {
    'use strict';

    angular
        .module("appMeli")
        .controller("HomeController",
        ["PersonaService",
            "$scope",
            HomeController])

    //Ahora

    function HomeController(PersonaService, $scope) {

        var vm = this;

        PersonaService.query(function (data) {
            vm.personas = data;
            },
            function (response) {
                vm.msj = "Error! "+response.statusText;
            });
    }




    //Antes

    //function HomeController($http, personasService, $scope) {

    //    var onUserComplete = function (data) {
    //        $scope.personas = data;
    //    };
    //        personasService.getpersonas().then(onUserComplete, onError);
    //        var onError = function (reason) {

    //            $scope.error = "Error al cargar los datos";
    //        };
    //    };

    //    var onPersonas = function (data) {
    //        $scope.personas = data;
    //    };
}());