﻿(function () {
    'use strict';

    angular
        .module("appMeli")
        .controller("meliMenuController",
        ["$scope",
            meliMenuController]
        );

    function meliMenuController($scope) {

        $scope.showMenu = true;

        this.getActiveElement = function () {
            return $scope.activeElement;
        };

        this.setActiveElement = function (el) {
            $scope.activeElement = el;
        };

        $scope.$on('meli-menu-show', function (evt, data) {
            $scope.showMenu = data.show;
        });

    }

}());