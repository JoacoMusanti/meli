﻿(function () {
    'use strict';

    angular
        .module("appMeli")
        .controller("DashboardController",
        ["$scope",
            DashboardController])

    //Ahora

    function DashboardController($scope) {

        var vm = this;

        vm.gridsterOpts = {
            columns: 12,
            margins: [20, 20]
        }

        vm.charts = [
            {
                title: 'Grafico 1',
                sizeX: 3,
                sizeY: 3,
                row: 0,
                col: 0
            },
            {
                title: 'Grafico 2',
                sizeX: 2,
                sizeY: 2,
                row: 0,
                col: 2
            }
        ];
    }
}());