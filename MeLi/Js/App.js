﻿(function () {
    'use strict';

    var app = angular.module("appMeli",
        [
            "ui.router",
            "ngAnimate",
            'ngSanitize',
            "ui.bootstrap",
            "commonServices",
            "gridster"
        ]);

    app.config(function ($provide) {
        $provide.decorator("$exceptionHandler",
            ["$delegate",
                function ($delegate) {
                    return function (exception, cause) {
                        exception.message = "Por favor contactese con el equipo desarrollador. " +
                            exception.message;
                        $delegate(exception, cause);
                        alert(exception.message);
                    };
                }]);
    });

    app.config(["$locationProvider", function ($locationProvider) {
        $locationProvider.hashPrefix('');
    }]);


    app.config(["$stateProvider",
        "$urlRouterProvider",
        function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise("/");

            $stateProvider
                .state("login", {
                    url: "/login",
                    templateUrl: "Js/Templates/Login.html",
                    controller: "LoginController",
                    controllerAs: 'login'
                })

                .state("home", {
                    url: "/home",
                    templateUrl: "Js/Templates/Home.html"
                })

                .state("personas", {
                    url: "/personas",
                    templateUrl: "Js/Templates/Personas.html",
                    controller: "HomeController",
                    controllerAs: "home"
                })

                .state("dashboard", {
                    url: "/dashboard",
                    templateUrl: "Js/Templates/Dashboard.html",
                    controller: "DashboardController",
                    controllerAs: "dash"
                })

        }]
    );

}());