﻿(function () {
    'use strict';

    angular
        .module("commonServices")
        .factory("PersonaService",
            ["$resource",
            PersonaService])

    function PersonaService($resource) {
        return $resource("api/prueba/:id");
    }

}());