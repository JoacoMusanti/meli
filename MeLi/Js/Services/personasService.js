﻿(function () {
    "use strict";
    angular
        .module("appMeli")
        .factory('personasService', function ($http) {

            var getpersonas = function () {
                return $http.get("/api/prueba")
                    .then(function (response) {
                        return response.data;
                    });
            };

            return { getpersonas: getpersonas };
    });
}());