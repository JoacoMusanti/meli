﻿(function () {
    'use strict';

    angular
        .module("appMeli")
        .directive("meliFramework", function () {
            return {
                transclude: true,
                scope: {
                    title: '@',
                    subtitle: '@',
                    iconFile: '@',
                },
                controller: "meliFrameworkController",
                templateUrl: "Js/Templates/meliFrameworkTemplate.html"
            };
        });
}());