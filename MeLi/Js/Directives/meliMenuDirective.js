﻿(function () {
    'use strict';

    angular
        .module("appMeli")
        .directive("meliMenu", function () {
            return {
                transclude: true,
                scope: {

                },
                controller: "meliMenuController",
                templateUrl: "Js/Templates/meliMenuTemplate.html",
                link: function (scope, el, attr) {

                }
            };
        });
}());