﻿(function () {
    'use strict';

    angular
        .module("appMeli")
        .directive("meliMenuItem", function () {
            return {
                require: '^meliMenu',
                scope: {
                    label: '@',
                    icon: '@',
                    route: '@',
                },
                templateUrl: "Js/Templates/meliMenuItemTemplate.html",
                //controller: "meliMenuController",
                controller: "meliFrameworkController",
                link: function (scope, el, attr, ctrl) {

                    scope.isActive = function () {
                        return el === ctrl.getActiveElement();
                    };

                    el.on('click', function (evt) {
                        evt.stopPropagation();
                        evt.preventDefault();
                        $($window).trigger('resize');
                        scope.$apply(function () {
                            ctrl.setActiveElement(el);
                        });
                    });
                }
            };
        });
}());