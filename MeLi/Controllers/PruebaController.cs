﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace resttest.Controllers
{
    public class Persona
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Caracteristica { get; set; }
    }

    [Authorize]
    public class PruebaController : ApiController
    {
        public List<Persona> Get()
        {
            var a = new List<Persona>();

            var b = new Persona()
            {
                Nombre = "joaco",
                Apellido = "musanti",
                Caracteristica = "crack en todo",
                FechaNacimiento = new DateTime(1994, 9, 4)
            };

            var c = new Persona()
            {
                Nombre = "nicole",
                Apellido = "schmidt",
                Caracteristica = "trola",
                FechaNacimiento = new DateTime(1994, 8, 5)
            };

            a.Add(b);
            a.Add(c);


            return a;
        }
    }
}
